//DOM (Document Object Model) Manipulation

//Getting HTML Elements
document.getElementById("demo");
document.getElementsByTagName("h1");
document.getElementsByClassName("title");

document.querySelector("#demo");
document.querySelector("h1");
document.querySelector(".title");

//Change HTML Elements

	//Properties:
		//element.innerHTML = new content
		//element.style.property

	//Methods
		//element.setAttribute
		//element.removeAttribute

document.getElementsByTagName("h1").innerHTML = 'Hello World';

document.getElementById("demo").setAttribute("class", "sample");

document.getElementById("demo").removeAttribute("class", "sample");

//Event Listener

	//Syntax:
		//element.addEventListener("<event>", (<callback>) => {})

let firstName = document.querySelector("#txt-first-name");
let lastName = document.querySelector("#txt-last-name");
let fullName = document.querySelector("#span-full-name");

/*let a = "";
let b = "";

firstName.addEventListener("keyup", (event) => {

	a = event.target.value;
	fullName.innerHTML = `${a} ${b}`;
})

lastName.addEventListener("keyup", (event) => {
	console.log(event);

	b = event.target.value;
	fullName.innerHTML = `${a} ${b}`;	
})*/



const getFullName = () => {
		let fName = firstName.value;
		let lName = lastName.value;

		fullName.innerHTML = `${fName} ${lName}`;
} 

firstName.addEventListener("keyup", getFullName)
lastName.addEventListener("keyup", getFullName);

